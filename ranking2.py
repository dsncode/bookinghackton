
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:      TAIPEI BOOKING.COM HACKATON
#
# Author:      kru
#
# Created:     11/03/2017
# Copyright:   (c) kru 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

import pprint
#from scipy.stats import pearsonr,kendalltau
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from flask import Flask

from flask import request
import json


import numpy as np

app = Flask(__name__)

MIN_RET = 10
WEIGHT_OF_RATING = 0.1



def createBid(uuid):

    client = MongoClient('mongodb://10.187.1.187:27017/')
    bid_db = client['bid']
    bids_col = bid_db['bids']
    #bids_col.insert(
    content = request.get_json(silent=True)
    print content
    return uuid

    t =1-r0p
    t=2



def getCandidates(bidID):
    client = MongoClient('mongodb://10.187.1.187:27017/')
    bid_db = client['bid']
    bids_col = bid_db['bids']
    #pprint.pprint(bids_col.find_one({"_id": 1}))
    bid = bids_col.find_one({"_id": bidID})
    print bid
    print type(bid)



#def calculateHotelsScore(bidID,inputDB,outputDB):


@app.route('/api/add_message/<uuid>', methods=['GET', 'POST'])
def calculateHotelsScore(uuid):
     # DO THE FLASK THANG
    #req = request.json
    req= { "bidID":"58c4436dbf3943b4cc969123",
            "inputDB":"hotels",
            "outputDB":"hotelCandidates"}

    bidID= req['bidID']
    inputDB=req['inputDB']
    outputDB=req['outputDB']
    #

    if isinstance(bidID,str):
        print bidID + " IS STRING"
        #bidID = ObjectId(bidID)
        #print "BIDID " + bidID

    client = MongoClient('mongodb://45.77.19.59:27017/')
    db = client['hackton']
    hotelsAll = db[inputDB]
    bidsAll = db['bids']
    #hotel = hotelsAll.find_one({"_id": hotelID})
    #bid = bidsAll.find_one({"_id":bidID})
    bid = bidsAll.find_one({"_id":ObjectId(bidID)})
    #print  "bid "+bid

    req =[n['facility_id']  for  n in bid['requirements']]
    w =  [n['w']  for  n in bid['requirements']]



    #debughf=[]
    hotelsAllfind=hotelsAll.find(modifiers = {"hotel_facilities":{"$in":req }})  # TODO:::: actually we should query not for all but for those who meet at least one requirement +return  on no results




    hotels_matrix=np.zeros([hotelsAllfind.count(),len(req)])
    hotels_matrix = np.vstack((hotels_matrix.T,np.zeros(hotels_matrix.shape[0]))).T  ##### ADDING CUSTOMER RATING TO OVERALL RATING
    hid=hotelsAllfind.count()*[0]
    for hi,h in enumerate(hotelsAllfind):
        hf = h['hotel_facilities_filtered']
        hid[hi]= h['hotel_id']
        #debughf.extend(hf)
        for  ri,r in enumerate(req):
            hotels_matrix[hi,ri] = w[ri] if r in hf else 0

        hotels_matrix[hi,len(req)] = h['review_score']

    ##### ADDING CUSTOMER RATING TO OVERALL RATING CD...
    req.append(10)# desired rating entry the best one possible on the website
    w.append(WEIGHT_OF_RATING)






    #####
    rankings = np.zeros(hotels_matrix.shape[0])

    for rowi,row in enumerate(hotels_matrix):
        rankings[rowi]=np.dot(row, w) / (np.linalg.norm(row) * np.linalg.norm(w))

    #omg this is so ugly
    rankings2=  np.array( filter(lambda v: v==v, list(dict.fromkeys(rankings))))
    rankings2.sort()
    rankings2 = rankings2[::-1]

    # return the list of indexes to hotel entries

    i=0
    ret=[]
    while len(ret)<MIN_RET:
        try:
            retindices=np.where(rankings==rankings2[i])[0]
            ret += [hid[r] for r in retindices]
            i+=1
        except IndexError, error:
            break

    # put to the output db
    outputCol=db[outputDB]
    t= 0


    retFull = [  hotelsAll.find_one(int(k)) for k in ret ]
    #update some fields
    for irf, rf in enumerate(retFull):
        rf["RANK"] = irf
        rf["BID"] = bidID

    outputCol.remove()
    outputCol.insert_many(retFull)

    return "Done!"
    #return ret






def main():
    #TESTS
    d = calculateHotelsScore(ObjectId("58c4436dbf3943b4cc969123"),'hotels','hotelCandidates')
    print d
    e = calculateHotelsScore(ObjectId("58c4436dbf3943b4cc969123"),'hotelCandidates','hotelCandidates')
    print e

if __name__ == '__main__':
    main()