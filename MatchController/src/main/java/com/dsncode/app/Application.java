package com.dsncode.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
@EnableAutoConfiguration
@ComponentScan("com.dsncode.app*")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}

}
