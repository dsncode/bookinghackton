package com.dsncode.app;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dsncode.mongo.SMONGO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

	@RestController
	@EnableAutoConfiguration
	@RequestMapping("/company")
	public class CompanyController {
		private static ObjectMapper om = new ObjectMapper();
		
	    public CompanyController()
	    {
	          
	    }
	    
	    @RequestMapping(value="/allfacilities", method = RequestMethod.GET)	    
	    public static JsonNode getFacilities() {
	    	ObjectNode facilities = om.createObjectNode();
	    	facilities.put("data", 1);
	    	return facilities;
	    }
	    	    
	    @RequestMapping(value="/bids/save", method = RequestMethod.POST)	    
	    public static JsonNode getbids(@RequestBody JsonNode request) {
	    	
	    	long date_long = request.path("when").asLong();
	    	BasicDBObject bid = (BasicDBObject) JSON.parse(request.toString());
	    	bid.put("when", new Date(date_long));
	    	bid.put("timestamp", new Date());
	    	
	    	
	    	BasicDBList req = (BasicDBList) bid.get("requirements");
	    	
	    	double max = req.size();
	    	double x = max;
	    	Iterator<Object> it = req.iterator();
	    	BasicDBList n_req = new BasicDBList();
	    	while(it.hasNext())
	    	{
	    		int ref = (Integer) it.next();
	    		BasicDBObject sample = new BasicDBObject();
	    		sample.put("facility_id", ref);
	    		sample.put("weigth", x/max);
	    		n_req.add(sample);
	    		x--;	
	    	}
	    	
	    	bid.put("requirements", n_req);
	    	
	    	System.out.println(bid);
	    	SMONGO.getInstance().getCollection("bids").insert(bid);
	    	return om.createObjectNode();
	    }
	    
	    @RequestMapping(value="/deals/{companyid}", method = RequestMethod.GET)	    
	    public static JsonNode getDeals(@PathVariable String companyid) throws JsonProcessingException, IOException {
	        
	    	ObjectNode deals = om.createObjectNode();
	    	return om.readTree(new File("/home/daniel/hackaton_meta/deals.json"));
	    }
	    
	    

	}
