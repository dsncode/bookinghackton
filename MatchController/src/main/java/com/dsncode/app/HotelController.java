package com.dsncode.app;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dsncode.mongo.SMONGO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

	@RestController
	@EnableAutoConfiguration
	@RequestMapping("/hotel")
	public class HotelController {
		private static ObjectMapper om = new ObjectMapper();
		private static String collection = "hotel";
	    public HotelController()
	    {
	          
	    }
	    
	    @RequestMapping(value="/allfacilities", method = RequestMethod.GET)	    
	    public static JsonNode getFacilities() {
	    	ObjectNode facilities = om.createObjectNode();
	    	facilities.put("data", 1);
	    	return facilities;
	    }
	    
	    @RequestMapping(value="/bids/available/{hotelid}", method = RequestMethod.GET)	    
	    public static JsonNode getbids(@PathVariable String hotelid) throws JsonProcessingException, IOException {
	    	
	    	return om.readTree(new File("/home/daniel/hackaton_meta/bids.json"));
	    }
	    
	    @RequestMapping(value="/deals/save", method = RequestMethod.POST)	    
	    public static JsonNode getbids(@RequestBody JsonNode request) {
	        
	    	long date_long = request.path("deal_expiration_date").asLong();
	    	BasicDBObject deal = (BasicDBObject) JSON.parse(request.toString());
	    	deal.put("deal_expiration_date", new Date(date_long));
	    	deal.put("timestamp", new Date());
	    	
	    	SMONGO.getInstance().getCollection("deals").insert(deal);
	    	
	    	return om.createObjectNode();
	    }
	    

	}
