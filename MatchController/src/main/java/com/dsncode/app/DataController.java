package com.dsncode.app;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

	@RestController
	@EnableAutoConfiguration
	@RequestMapping("/integration")
	public class DataController {
		private static ObjectMapper om = new ObjectMapper();
	    public DataController()
	    {
	          
	    }
	    
	    @RequestMapping(value="/allfacilities", method = RequestMethod.GET)	    
	    public static JsonNode getFacilities() {
	    	ObjectNode facilities = om.createObjectNode();
	    	facilities.put("data", 1);
	    	return facilities;
	    }
	    
	    
	    

	}
