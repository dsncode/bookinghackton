package com.dsncode.integration

import java.io.File

import scala.collection.JavaConversions.asScalaBuffer

import com.dsncode.mongo.SMONGO
import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.BasicDBObject

object FacilitiesExtractor extends App{
  
  
  val om = new ObjectMapper();
  val json = om.createObjectNode();
  
  val full_json = SMONGO.getInstance.getCollection("facilities").find()
  .toArray()
  .map(_.asInstanceOf[BasicDBObject])
  .filter { x => x.getString("facilitytype_id").contentEquals("") }
  .foldLeft(json)((json,doc)=>{
    
    val hotelfacility_id = doc.getString("hotelfacilitytype_id")
//    val facilitytype_id = doc.getString("facilitytype_id")
    
    json.put(hotelfacility_id, doc.getString("name"))
    json
  })
  
  om.writeValue(new File("D:\\workspaces\\facilities.json"), json)
  println("ok")
  
}