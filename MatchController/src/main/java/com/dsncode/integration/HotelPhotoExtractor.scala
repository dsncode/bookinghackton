package com.dsncode.integration

import scala.collection.JavaConversions.asScalaBuffer

import com.dsncode.http.client.RequestSender
import com.dsncode.mongo.SMONGO
import com.mongodb.BasicDBObject
import com.mongodb.util.JSON
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.mongodb.DBObject

object HotelPhotoExtractor extends App{
  
    val sender = new RequestSender("hacker234","8hqNW6HtfU")

  
  val mapped = SMONGO.getInstance.getCollection("hotels").find().toArray().toList.par.map(_.asInstanceOf[BasicDBObject])
  .foreach { hotel => 
    
  val id = hotel.getInt("_id");
  println("parsing:"+id)
  val hotel_meta = sender.getAsJson("https://distribution-xml.booking.com/json/bookings.getHotelDescriptionPhotos?hotel_ids="+id);
  
  val photos = JSON.parse(hotel_meta.toString()).asInstanceOf[DBObject];
  val ref = SMONGO.getInstance.getCollection("hotels").findOne(new BasicDBObject("_id",id));
  ref.put("photos", photos)
  SMONGO.getInstance.getCollection("hotels").update(new BasicDBObject("_id",id), ref)
  
  }

  
    println("ok")
    
}