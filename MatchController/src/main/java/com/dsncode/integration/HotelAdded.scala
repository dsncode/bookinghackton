package com.dsncode.integration

import com.dsncode.http.client.RequestSender
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.mongodb.BasicDBObject
import com.dsncode.mongo.SMONGO

object HotelAdded extends App{
  
  
  val sender = new RequestSender("hacker234","8hqNW6HtfU")
  
  val json = sender.getAsJson("https://distribution-xml.booking.com/json/bookings.getHotelDescriptionTypes?languagecodes=en");
  val data = json.iterator().toList.map { x => 
    
    val doc = new BasicDBObject()
    println(x)
    doc.put("_id", x.path("descriptiontype_id").asInt())
    doc.put("name", x.get("name").asText())
    
  doc
  }.toList
  
  SMONGO.getInstance.getCollection("facilities_type").insert(data)
  
  
}