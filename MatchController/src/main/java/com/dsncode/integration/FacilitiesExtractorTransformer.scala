package com.dsncode.integration

import java.io.File

import scala.collection.JavaConversions.asScalaBuffer

import com.dsncode.mongo.SMONGO
import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.BasicDBObject

object FacilitiesExtractorTransformer extends App{
  
  
  val om = new ObjectMapper();
  val json = om.createObjectNode();
  
  val full_json = SMONGO.getInstance.getCollection("facilities").find()
  .toArray()
  .map(_.asInstanceOf[BasicDBObject])
  println("ok")
  
}